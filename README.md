# SHELL
Questo progetto racchiude una serie di shell di uso comune

## # fix-wp-permissions.sh
Questa shell permette di riparare i permessi delle directory e dei file di wordpress.

### Parametri
1. wordpress root directory
2. wordpress owner
3. wordpress group 
4. webserver group

### Esempio
sh fix-wp-permissions.sh . sanimem psacln psaserv

## # backup_mac.sh
Questa shell permette di effettuare un backup di un DB su un MAC

### Parametri
1. nome db
2. nome file destinazione (senza .sql)

### Esempio
sh backup_mac.sh globestar filedestinazione

## # restore_mac.sh
Questa shell permette di effettuare un restore di un DB su un MAC

### Parametri
1. nome db
2. nome file sorgente (senza .sql)

### Esempio
sh restore_mac.sh globestar filesorgente
