#!/bin/bash
#
# This script configures WordPress file permissions based on recommendations
# from http://codex.wordpress.org/Hardening_WordPress#File_permissions
#
# Author: Michael Conigliaro <mike [at] conigliaro [dot] org>
#
# Example : sh fix-wp.sh . sanimem psacln psaserv
#

if [ $# -eq 0 ]
  then
    echo "Inserire argomenti"
    echo "fix-wp.sh . owner wordpress-group webserver-group"
    echo "sh fix-wp.sh sanimem.it/httpdocs sanimem psacln psaserv"
    exit
fi

WP_ROOT=$1 # <-- wordpress root directory
WP_OWNER=$2 # <-- wordpress owner
WP_GROUP=$3 # <-- wordpress group
WS_GROUP=$4 # <-- webserver group

echo "fix-wp START Plesk"
# reset to safe defaults
echo "reset to safe defaults"
find ${WP_ROOT} -exec chown ${WP_OWNER}:${WP_GROUP} {} \;
find ${WP_ROOT} -type d -exec chmod 755 {} \;
find ${WP_ROOT} -type f -exec chmod 644 {} \;

# allow wordpress to manage wp-config.php (but prevent world access)
echo "allow wordpress to manage wp-config.php"
chgrp ${WS_GROUP} ${WP_ROOT}/wp-config.php
chmod 444 ${WP_ROOT}/wp-config.php
chmod 444 ${WP_ROOT}/.htaccess

# allow wordpress to manage wp-content
echo "allow wordpress to manage wp-content"
find ${WP_ROOT}/wp-content -exec chgrp ${WS_GROUP} {} \;
find ${WP_ROOT}/wp-content -type d -exec chmod 755 {} \;
find ${WP_ROOT}/wp-content -type f -exec chmod 644 {} \;

echo "fix-wp END"