#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    echo "Inserire argomenti"
    echo "sh restore_mac.sh database filesorgente"
    exit
fi

source="./$2.sql"
/usr/local/mysql-5.7.10-osx10.9-x86_64/bin/mysql --database=$1 --user=root --password=l0t0 < $source
